using System;
using System.Collections.Generic;
using System.Linq;

public class QueryExecution
{
    public int DeferredExecution()
    {
        List<Product> productData = DataSource.DataSource.GetProductList().Take(6).ToList();
        int i = 0;
        var products = from pdt in productData
                       select ++i;
        for(int j =0; j< products.Count<int>(); j++)
        {
            Console.WriteLine($"Value of i: {i} --- ");
        }
        return 0;
    }

    public int QuickExecution()
    {
        int[] numbers = { 3, 4, 2, 3, 7, 8, 5, 3, 2, 7, 8 };
        int i = 0;
        var numberQuery = (from num in numbers
                          select ++i).ToArray();
        foreach (var num in numberQuery)
        {
            Console.WriteLine($"Value of i: {i} ---- Data: {num}");
        }
        return 0;
    }

    public int ReusingQueryExecution()
    {
        int[] numbers = { 3, 4, 2, 3, 7, 8, 5, 3, 2, 7, 8 };

        var numberQuery = from num in numbers
                          where num > 5
                          select num;
        Console.WriteLine("Before Modifying the array");
        foreach (var num in numberQuery)
        {
            Console.WriteLine($"Data: {num}");
        }
        numbers[3] = 14; 
        Console.WriteLine("After Modifying the array");
        foreach (var num in numberQuery)
        {
            Console.WriteLine($"Data: {num}");
        }

        return 0;
    }

}