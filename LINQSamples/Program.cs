﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.Marshalling;

namespace LINQSamples;

class Program
{
    public static void Main(string[] args)
    {

        // RestictionsExamples();
        // SelectExamples();
        // OrderingExamples();
        // GroupingExamples();
        // SetOperations();
        // QueryExecution();
        // AggregationExamples();
        JoinExamples();
    }

    public static void RestictionsExamples()
    {
        ShowExample(new Restrictions().ProductOutOfStock, "Poducts out of stock");
        ShowExample(new Restrictions().CostlyBooks, "Costly Books");
        ShowExample(new Restrictions().CheapBooks, "Cheap Books");
        ShowExample(new Restrictions().WordAndValue, "Word and their value");
    }

    public static void SelectExamples()
    {
        ShowExample(new Projections().SelectBasic, "Basic Select Command");
        ShowExample(new Projections().SelectProperty, "Select individual properties");
        ShowExample(new Projections().SelectMultiplePropertyAnonymous, "Select Multiple properties");
        ShowExample(new Projections().SelectMultiplePropertyUsingTuple, "Select Multiple properties Using tuple");
        ShowExample(new Projections().SelectPropertiesUsingSyntax, "Select Multiple properties using syntax");
        ShowExample(new Projections().SelectFromMultipleSequence, "Select From multiple sequence");
    }

    public static void OrderingExamples()
    {
        ShowExample(new Orderings().OrderByAphabeticalOrder, "Alpabetical order");
        ShowExample(new Orderings().OrderByProperty, "Order by property");
        ShowExample(new Orderings().OrderByAlphabeticalCustomComparator, "Order by using custom comparator");
        ShowExample(new Orderings().OrderDecendingInAlphbaticalOrder, "Order by using custom comparator");
        ShowExample(new Orderings().SortBooksByNameAndDate, "Using thenby operator.");
        ShowExample(new Orderings().SortWordsByLengthAndAlpha, "Sorting using 2 comparators.");
    }   

    public static void GroupingExamples(){
        ShowExample(new Grouping().BasicGrouping, "Grouping Number that produces different reminder when divided by 3");
        ShowExample(new Grouping().GroupByProperty, "Grouping by a property");
        ShowExample(new Grouping().NestedGrouping, "Nested Grouping");
        ShowExample(new Grouping().NestedGroupingMethodSyntax, "Nested Grouping Using method syntax");
    }
    public static void SetOperations(){
        ShowExample(new SetOperations().DistinctCommand, "Basic Distict usage");
        ShowExample(new SetOperations().DistinctProperties, "Basic Distict usage based on properties");
        ShowExample(new SetOperations().UnionSyntax, "Basic usage of union command Notice only the unique values are printed");
    }

    public static void QueryExecution(){
        ShowExample(new QueryExecution().DeferredExecution, "An Example for differed execution");
        ShowExample(new QueryExecution().QuickExecution, "An Example for Sudden execution");
        ShowExample(new QueryExecution().ReusingQueryExecution, "An example for reusing execution");
    }
    public static void AggregationExamples(){
        ShowExample(new AggregataionOperation().MinInGroups, "An Example for minimum item in groups.");
        ShowExample(new AggregataionOperation().SeededAggregate, "Seeded Aggregate example");
    }
    public static void JoinExamples(){
        ShowExample(new JoinOperation().CrossJoinQuery, "Basic Join Exmaple");
        ShowExample(new JoinOperation().LeftOuterJoin, "Example for left outer join");

    }


    private static void ShowExample(Func<int> exampleFunction, string exampleName)
    {
        Console.WriteLine($"Example Name: {exampleName}");
        exampleFunction();
        Console.WriteLine("Press Enter for next ");
        Console.ReadLine();
    }
}