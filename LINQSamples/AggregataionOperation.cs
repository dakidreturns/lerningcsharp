using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQSamples;

class AggregataionOperation{
    public int MinInGroups(){
        List<Product> products = DataSource.DataSource.GetProductList();
        var minGroups = from data in products
                        group data by data.Category into pg
                        let minPrice = pg.Min(pg=>pg.UnitPrice)
                        select (Category: pg.Key, Items: (from item in pg where item.UnitPrice == minPrice select item) );
        foreach(var group in minGroups){
            Console.WriteLine($"{group.Category} -------- ");
            foreach(var product in group.Items){
                Console.WriteLine($"{product}");
            }
        }
        return 0;
    }

    public int SeededAggregate(){
        /*
        The Aggreagte function is used to perform an operation across all the values in the given range.
        The seed is used to specify an initial value 
        */
        List<Product> product = DataSource.DataSource.GetProductList();
        decimal AccountBalance = 200.00m;
        // I am buying all the beverage  what will my remaining balance will be.

        
        var remianingBalance = (from bev in product
                                where bev.Category == "Beverages" && bev.UnitsInStock >0
                                select bev.UnitPrice).Aggregate(AccountBalance, (remBal,b)=> {
                                    Console.WriteLine($"{remBal} - {b}"); 
                                    
                                    return (remBal-b) > 0 ? remBal-b : remBal;});
        
        Console.WriteLine($"Remaining Balance if I buy all units in stock {remianingBalance}");

        return 0;
    }
}