using System;
using System.Net.Sockets;

namespace DataSource;

public class Book{
    public string Id {get;}
    public string Author {get; set;} = "";
    public string Title {get; set;} = "";
    public string Genre{get;set;} = "";
    public float Price{get;set;} = 0.00f;
    public DateTime PublishDate {get; set;} = new DateTime();
    public string Description{get; set;} = "";

    public Book(string id, string author, string title, string genre, string price, string publishDate, string description ){
        Id = id;
        Author = author;
        Title = title;
        Genre = genre;
        Price = float.Parse(price);
        PublishDate = DateTime.Parse(publishDate);
        Description = description;
    }
    public Book(string id){
        Id = id;
    }
    public Book(){
        Id = "";
    }
    public override string ToString()
    {
        return $"Title = {Title}, Author = {Author}, Genre = {Genre}\nPrice = {Price}\nPublish date = {PublishDate}";
    }
}