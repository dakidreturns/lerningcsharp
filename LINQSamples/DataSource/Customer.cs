using System;
using System.Collections.Generic;
public class Customer{
    public string CustomerID {get; set;} = "";
    public string CompanyName{get;set;} = "";
    public string ContactName{get;set;} = "";
    public string ContactTitle{get;set;} = "";
    public string Phone{get;set;} = "";
    public string Fax{get;set;} = "";
    public Address address {get;} = new Address();
    public List<Order> Orders { get; set; } = new List<Order>();
    public override string ToString()
    {
        return $"ID:{CustomerID}\nName: {CompanyName}\n ContactName: {ContactName}\n ContactTitle: {ContactTitle}\n Phone: {Phone}\n Address: {address}\n Orders: {Orders.Count}";
    }
}

    public class Order
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Total { get; set; }
        public override string ToString() => $"{OrderID}: {OrderDate:d} for {Total}";
    }


public class Address{
    public string AddressStr{get;set;} = "";
    public string City{get;set;} = "";
    public string Region{get; set;} = "";
    public string PostalCode{get;set;} = "";
    public string ContactTitle{get;set;} = "";
    public string Country{get; set;} = "";
    public override string ToString()
    {
        return $"{AddressStr}\n City: {City}\n Region: {Region}\n PostalCode: {PostalCode}\n Country: {Country}\n\n";
    }
}