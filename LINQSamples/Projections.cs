using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using DataSource;

namespace LINQSamples;

public class Projections{
    public int SelectBasic(){
        int[] numbers = {5,4,3,5,6,4,2,3,5,7,5,4,0};
        var numbersPlusOne = from n in numbers
                              select n+1;
        foreach(int num in numbersPlusOne){
            Console.Write($"{num} ");
        }
        Console.WriteLine("");
        return 0;
    }

    public int SelectProperty(){
        List<Book> books = DataSource.DataSource.GetBooksData();
        
        var bookTitles = from book in books
                        select book.Title;
        
        foreach (var bookName in bookTitles){
            Console.WriteLine(bookName);
        }
        return 0;
    }
    public int SelectMultiplePropertyAnonymous(){
        List<Book> books = DataSource.DataSource.GetBooksData();
        
        var bookTitlesAndAuthor = from book in books
                        select new {book.Title, book.Author};
        
        foreach (var book in bookTitlesAndAuthor){
            Console.WriteLine($"{book.Title} Authored by: \n\t {book.Author}");
        }
        return 0;
    }
    public int SelectMultiplePropertyUsingTuple(){
        List<Product> products = DataSource.DataSource.GetProductList();
        
        var productsAndPrices = from pdt in products
                        select (Name: pdt.ProductName, Price: pdt.UnitPrice, pdt.UnitsInStock);
        
        foreach (var product in productsAndPrices){
            Console.WriteLine($"Product: {product.Name} Price: {product.Price} Remaining Qty: {product.UnitsInStock}");
        }
        return 0;
    }
    public int SelectPropertiesUsingSyntax(){
        List<Product> products = DataSource.DataSource.GetProductList();
        
        var productsAndPrices = products.Where((pdt)=>pdt.UnitsInStock > 100)
                                        .Select((product) => (Name: product.ProductName, Price: product.UnitPrice, UnitsInStock: product.UnitsInStock));
        
        foreach (var pdt in productsAndPrices){
            Console.WriteLine($"Product: {pdt.Name} Price: {pdt.Price} Remaining Qty: {pdt.UnitsInStock}");
        }
        return 0;
    }
    public int SelectFromMultipleSequence(){
        int[] numbersA = {3,2,3,4,5,2,1,8,7,5,3,6,6};
        int[] numbersB = {4,3,5,9,5,2,7,9,0,4,6,7,4,2,3,5,5};

        // int count = 0;
        var pairs = from a in numbersA
                    from b in numbersB 
                    where a < b
                    select(a,b);

        var pairs2 = numbersA.SelectMany(b => numbersB , (a,b) => new {a,b}).Where((data)=> data.a<data.b );
        
        foreach (var pair in pairs2){
            Console.WriteLine($"{pair.a} is less than {pair.b}");
        }

        return 0;
    }
    
}