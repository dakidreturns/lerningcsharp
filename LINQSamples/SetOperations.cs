using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public class SetOperations
{
    public int DistinctCommand()
    {
        int[] numbers = { 94, 4, 2, 2, 4, 0, 2, 35, 7, 67, 67, 4, 1, 23, 4, 3, 2, 3, 4, 2, };
        var distintNumbers = numbers.Distinct();
        foreach (int i in distintNumbers)
        {
            Console.Write($"{i} ");
        }
        Console.WriteLine("");

        return 0;
    }

    public int DistinctProperties()
    {
        List<Product> products = DataSource.DataSource.GetProductList();
        var distinctCategories = products.Select(p => p.Category).Distinct();

        foreach (var productCategory in distinctCategories)
        {
            Console.WriteLine($"{productCategory}");
        }
        Console.WriteLine("");

        return 0;
    }

    public int UnionSyntax()
    {
        int[] numberSetA = { 9, 3, 4, 2, 1, 5, 7, 0, 3 };
        int[] numberSetB = { 7, 0, 3, 5, 3, 1, 3, 5 };

        var numberSetUnion = numberSetA.Union(numberSetB);

        foreach (var num in numberSetUnion)
        {
            Console.Write($"{num} ");
        }
        Console.WriteLine("");

        return 0;
    }
    
}