using System;
using System.Collections.Generic;
using System.Linq;

public class CaseSensitiveComparer : IComparer<string>
{
    public int Compare(string? x, string? y) => string.Compare(x, y, StringComparison.Ordinal);
}

public class Orderings
{
    public int OrderByAphabeticalOrder()
    {
        string[] words = { "An", "offer", "He", "cant", "refuse" };
        var sortedWords = from word in words
                          orderby word
                          select word;
        Console.WriteLine("Sorted List of words: ");
        foreach (var w in sortedWords)
        {
            Console.Write($"{w} - ");
        }
        Console.WriteLine("");
        return 0;
    }

    public int OrderByProperty()
    {
        string[] words = { "An", "offer", "He", "cant", "refuse" };
        var sortedWords = from word in words
                          orderby word.Length
                          select word;
        Console.WriteLine("Sorted List of words: ");
        foreach (var w in sortedWords)
        {
            Console.WriteLine($"{w}");
        }
        Console.WriteLine("");
        return 0;
    }
    public int OrderByAlphabeticalCustomComparator()
    {
        string[] words = { "An", "offer", "He", "cant", "refuse" };
        var sortedWords = words.OrderBy(w => w, new CaseSensitiveComparer());
        Console.WriteLine("Sorted List of words (Case Sensitive): ");
        foreach (var w in sortedWords)
        {
            Console.Write($"{w} - ");
        }
        Console.WriteLine("");
        return 0;
    }
    public int OrderDecendingInAlphbaticalOrder()
    {
        string[] words = { "An", "offer", "He", "cant", "refuse" };
        var sortedWords = words.OrderByDescending(w => w, new CaseSensitiveComparer());
        Console.WriteLine("Reverse Sorted List of words (Case sensitive): ");
        foreach (var w in sortedWords)
        {
            Console.Write($"{w} - ");
        }
        Console.WriteLine("");
        return 0;
    }

    public int SortWordsByLengthAndAlpha()
    {
        string[] digits = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

        var sortedDigits = from digit in digits
                           orderby digit.Length, digit
                           select digit;

        Console.WriteLine("Sorted digits:");
        foreach (var d in sortedDigits)
        {
            Console.Write($"{d} ");
        }
        Console.WriteLine("");
        return 0;
    }

    public int SortBooksByNameAndDate()
    {
        List<DataSource.Book> books = DataSource.DataSource.GetBooksData();

        var sortedBooks = from book in books
                          orderby book.Title, book.PublishDate
                          select book;

        foreach (var book in sortedBooks)
        {
            Console.WriteLine($"{book.Title} {book.PublishDate}");
        }
        return 0;
    }
}