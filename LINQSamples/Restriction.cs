/**
These code contains usage of the Where operator to perform 
restrictions on the selection of objects.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using DataSource;


namespace LINQSamples;

/// <summary>
/// Create a new instance of this class to run the examples related to restirn
/// </summary>
class Restrictions
{
    public int ProductOutOfStock()
    {
        var products = DataSource.DataSource.GetProductList();
        var outOfStockProduct = from prod in products
                                where prod.UnitsInStock == 0
                                select prod;
        foreach (var pdt in outOfStockProduct)
        {
            Console.WriteLine($"{pdt}");
        }
        return 0;
    }

    public int CostlyBooks()
    {
        List<Book> books = DataSource.DataSource.GetBooksData();
        var costlyBooks = from book in books
                          where book.Price > 40
                          select book;
        foreach (var book in costlyBooks)
        {
            Console.WriteLine($"{book}");
        }
        return 0;
    }
    public int CheapBooks()
    {
        List<Book> books = DataSource.DataSource.GetBooksData();
        var costlyBooks = from book in books
                          where book.Price < 10
                          select book;
        foreach (var book in costlyBooks)
        {
            Console.WriteLine($"{book}");
        }
        return 0;
    }

    public int WordAndValue(){
        string[] digits = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        var shortDigits = digits.Where((digitName,index) => digitName.Length < index);

        foreach(var digit in shortDigits){
            Console.WriteLine($"{digit} is less number of characer than value");
        }

        return 0;
    }

}
