using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;

namespace LINQSamples;

class Grouping
{
    public int BasicGrouping()
    {
        int[] numbers = { 4, 3, 5, 9, 5, 2, 7, 9, 0, 4, 6, 7, 4, 2, 3, 5, 5 };

        var numberGroups = from n in numbers
                           group n by n % 3 into g
                           select (Remainder: g.Key, Numbers: g);

        foreach (var g in numberGroups)
        {
            Console.Write($"Numbers with a remainder of {g.Remainder} when divided by 3: ");
            foreach (var n in g.Numbers)
            {
                Console.Write($"{n} ");
            }
            Console.WriteLine("");
        }

        return 0;
    }

    public int GroupByProperty()
    {
        List<Product> products = DataSource.DataSource.GetProductList();

        var productGroup = from pdt in products
                           group pdt by pdt.Category into g
                           select (Category: g.Key, Products: g);

        foreach (var ordergroup in productGroup)
        {
            Console.WriteLine($"Products in Group {ordergroup.Category}");
            foreach (var product in ordergroup.Products)
            {
                Console.WriteLine($"{product}");
            }
            Console.WriteLine("");

        }
        return 0;
    }

    public int NestedGrouping()
    {
        List<Customer> customers = DataSource.DataSource.GetCustomerData();

        var yearGroupOrders = from customer in customers
                              select (
                                 Name: customer.CompanyName,
                                 YearGroups: from order in customer.Orders
                                             group order by order.OrderDate.Year into yg
                                             select (
                                                 Year: yg.Key,
                                                 MonthGroups: from yearOrders in yg
                                                              group yearOrders by yearOrders.OrderDate.Month into mg
                                                              select (
                                                                 Month: mg.Key, Orders: mg
                                                              )
                                             )
                              );

        foreach (var orders in yearGroupOrders)
        {
            Console.WriteLine($"Customer: {orders.Name}");
            foreach (var yearGroup in orders.YearGroups)
            {
                Console.WriteLine($"Orders in Year {yearGroup.Year}");
                foreach (var monthGroup in yearGroup.MonthGroups)
                {
                    Console.WriteLine($"\t{monthGroup.Month}");
                    foreach (var order in monthGroup.Orders)
                    {
                        Console.WriteLine($"\t\t{order}");
                    }
                }
            }
            Console.WriteLine("\n");
        }
        return 0;
    }
    public int NestedGroupingMethodSyntax()
    {
        List<Customer> customers = DataSource.DataSource.GetCustomerData();
        var yearGroupOrders = customers.Select(c => (Name: c.CompanyName,
                                                        YearGroups: c.Orders.GroupBy(
                                                        o => o.OrderDate.Year
                                                        ).Select(yg => (
                                                            Year: yg.Key,
                                                            MonthGroups: yg.GroupBy(o => o.OrderDate.Month)
                                                                      .Select(mg => (Month: mg.Key, Orders: mg))
                                                        ))
        ));

        foreach (var orders in yearGroupOrders)
        {
            Console.WriteLine($"Customer: {orders.Name}");
            foreach (var yearGroup in orders.YearGroups)
            {
                Console.WriteLine($"Orders in Year {yearGroup.Year}");
                foreach (var monthGroup in yearGroup.MonthGroups)
                {
                    Console.WriteLine($"\t{monthGroup.Month}");
                    foreach (var order in monthGroup.Orders)
                    {
                        Console.WriteLine($"\t\t{order}");
                    }
                }
            }
            Console.WriteLine("\n");
        }

        return 0;
    }

    public int GroupByCustomComparator()
    {
        string[] dataAnagrams = { "from   ", " salt", " earn ", "  last   ", " near ", " form  " };
        var anagrams = dataAnagrams.GroupBy(w => w.Trim(), a => a.ToUpper(), new AnagramComparator());
        foreach (var set in anagrams)
        {
            Console.WriteLine(set.Key);
            foreach (var word in set)
            {
                Console.WriteLine($"\t{word}");
            }
        }
        return 0;
    }
}

public class AnagramComparator : IEqualityComparer<string>
{
    public bool Equals(string? x, string? y)
    {
        if (x == null)
            x = "";
        if (y == null)
            y = "";
        return canonicalString(x) == canonicalString(y);
    }

    public int GetHashCode([DisallowNull] string obj) => canonicalString(obj).GetHashCode();
    private string canonicalString(string x)
    {
        char[] charactersArray = x.ToCharArray();
        Array.Sort<char>(charactersArray);
        return new string(charactersArray);
    }
}