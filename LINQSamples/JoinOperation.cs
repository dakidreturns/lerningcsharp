using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace LINQSamples;

class JoinOperation
{
    public int CrossJoinQuery()
    {
        string[] categories = {
                "Condiments",
                "Dairy Products",
                "Seafood",
                "Condiments"
        };
        List<Product> products = DataSource.DataSource.GetProductList();
        var productsInCategories = from c in categories
                                   join pdt in products on c equals pdt.Category
                                   select (Category: c, ProductName: pdt.ProductName, UnitsInStock: pdt.UnitsInStock);

        foreach (var prodCat in productsInCategories)
        {
            Console.WriteLine($"Category: {prodCat.Category} ProductName: {prodCat.ProductName}, UnitsInStock:{prodCat.UnitsInStock}");

        }

        return 0;
    }
    public int JoinLikeGroup()
    {
        string[] categories = {
                "Condiments",
                "Dairy Products",
                "Seafood",
                "Condiments"
        };
        List<Product> products = DataSource.DataSource.GetProductList();
        var productsInCategories = from c in categories
                                   join pdt in products on c equals pdt.Category into pc
                                   select (Category: c, Products: pc);

        foreach (var prodCat in productsInCategories)
        {
            Console.WriteLine($"Category: {prodCat.Category}");
            foreach (var pdt in prodCat.Products)
            {
                Console.WriteLine($"ProductName: {pdt.ProductName}, UnitsInStock:{pdt.UnitsInStock}");
            }
        }

        return 0;
    }

    public int LeftOuterJoin(){
        string[] categories = {
                "Condiments",
                "Dairy Products",
                "Seafood",
                "Pharma",
                "Vegitables",
                "Condiments"
        };
        List<Product> products = DataSource.DataSource.GetProductList();

        var leftOuterJoin = from c in categories
                            join pdt in products on c equals pdt.Category into ps 
                            from p in ps.DefaultIfEmpty()
                            select(Category: c , ProductName: (p == null) ? "(no Products)": p.ProductName);
        
        foreach(var pdtCat in leftOuterJoin){
            Console.WriteLine($"{pdtCat.ProductName} : {pdtCat.Category}");
        }

        return 0;
    }
}