using System;
using System.Threading;
using System.Threading.Tasks;

namespace TasksExample;
public class TaskBasics
{
    public static void PassingArgumentsPitfall1()
    {
        // Objects are passed by references hence the value might change.
        int[] sampleArray = [2, 1, 2, 3, 1, 2,];

        Task<int> taskSummation = Task<int>.Factory.StartNew(() => { return Sample.arraySum(sampleArray); });

        Task displayArray = Task.Factory.StartNew(() =>
        {
            Console.Write("The sum of array ");
            Sample.printArray(sampleArray);
            int sum = taskSummation.Result;
            Console.Write(" is: {0}\n", sum);
        });

        Thread.Sleep(2);
        sampleArray[1] = 9;

        taskSummation.Wait();
        displayArray.Wait();
    }

    public static void PassingArgumentsPitfall2()
    {
        // Passing variable to lambda function inside a loop
        // Task[] tasks = new Task[10];
        // for( int i=0; i < tasks.Length; i++){
        //     tasks[i] = Task.Factory.StartNew((object? obj) => {
        //         var data = new {
        //             Name = i,
        //             CreationTime = DateTime.Now.Ticks,
        //             ThreadNum = Thread.CurrentThread.ManagedThreadId
        //         };
        //         Console.WriteLine("Task #{0}, created at {1} on thread #{2}", data.Name, data.CreationTime, data.ThreadNum);
        //     }, i);
        // }
        // Task.WaitAll(tasks);

        // Fix: create an object with a copy of the required value.
        Task[] tasks = new Task[10];
        for (int i = 0; i < tasks.Length; i++)
        {
            tasks[i] = Task.Factory.StartNew((object? obj) =>
            {
                if (obj == null) return;

                if (obj is not SampleData nameObj) return; // Pattern matching

                Console.WriteLine("Task #{0}, created at {1} on thread #{2}", nameObj.Name, nameObj.CreationTime, nameObj.ThreadNum);

            }, new SampleData() { Name = i, CreationTime = DateTime.Now.Ticks, ThreadNum = Thread.CurrentThread.ManagedThreadId });
        }
        Task.WaitAll(tasks);
    }

    public static void StartReturnValueTasksExample()
    {
        int[] sampleArray = [2, 1, 2, 3, 1, 2,];

        Task<int> taskSummation = Task<int>.Factory.StartNew(() => { return Sample.arraySum(sampleArray); });

        Task displayArray = Task.Factory.StartNew(() =>
        {
            Console.Write("The sum of array ");
            Sample.printArray(sampleArray);
            int sum = taskSummation.Result;
            Console.Write(" is: {0}\n", sum);
        });

        taskSummation.Wait();
        displayArray.Wait();
    }

    public static void StartTasks()
    {
        Task taskA = new Task(() => Sample.productation(12));
        taskA.Start();

        Task taskB = Task.Run(() => Console.WriteLine("{0} Says: New Message here", Thread.CurrentThread.Name));

        Console.WriteLine("Hello from thread '{0}'", Thread.CurrentThread.Name);

        Action sumOfNumbers = () => Sample.summation(12);
        Task taskC = Task.Factory.StartNew(sumOfNumbers);

        taskA.Wait();
        taskB.Wait();
        taskC.Wait();
    }
}