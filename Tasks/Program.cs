﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace TasksExample;
internal class Program
{
    static void Main(string[] args)
    {
        //Console.WriteLine("This is the way");
        Thread.CurrentThread.Name = "Main";
        Console.WriteLine("Hallo ich bin ein sehr groβ program!!!! ");
        // TaskBasics.StartTasks();
        // TaskBasics.StartReturnValueTasksExample();
        // TaskBasics.PassingArgumentsPitfall2();

        // Task t = Task.Factory.StartNew(() => TasksCancellation.DoParallelStuffAsync() );
        // Console.WriteLine("Hehe I am running while watiing for task to be complete");

        // if (! t.Wait(2000)){
        //     Console.WriteLine("I am done waitng for you you took too long.... ");
        //     Environment.Exit(1);
        // }else{
        //     Console.WriteLine("Yay task completed....");
        // }

        // MultipleTokens.MultipleCancellationTokensFromSingleSource().Wait();
        TasksCancellation.UsingKeywordForToken().Wait();
    }
}