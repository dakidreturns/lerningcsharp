using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using TasksExample;

public class MultipleTokens
{
    public static async Task MultipleCancellationTokensFromSingleSource()
    {
        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        CancellationToken ctOne = cancellationTokenSource.Token;
        CancellationToken ctTwo = cancellationTokenSource.Token;

        Task t1 = Task.Factory.StartNew(() =>
        {
            while (!ctOne.IsCancellationRequested)
            {
                muchWork("Task 1");
            }
            ctOne.ThrowIfCancellationRequested();
        }, ctOne);

        Task t2 = Task.Factory.StartNew(() =>
        {
            while (!ctTwo.IsCancellationRequested)
            {
                muchWork("Task 2");
            }
            ctTwo.ThrowIfCancellationRequested();
        }, ctTwo);

        Sample.countDown(4);
        Console.WriteLine("-------------Cancelling tokens-------------");
        cancellationTokenSource.Cancel();

        try
        {
            await t1;
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine("{0}", e.Message);
        }

        try
        {
            await t2;
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine("{0}", e.Message);
        }

        Console.WriteLine("Disposing token....");
        cancellationTokenSource.Dispose();

    }
    private static void muchWork(string name)
    {
        Console.WriteLine("This is too much work for {0}", name);
        Thread.Sleep(500);
    }
}