using System;
using System.Threading;

namespace TasksExample;

public class SampleData
{
    public int Name;
    public long CreationTime;
    public int ThreadNum;
}

public class Sample
{
    public static void summation(int a)
    {
        int sum = 0;
        for (int i = 1; i <= a; i++)
        {
            sum += i;
            Thread.Sleep(500);
        }
        Console.WriteLine("{2} Says: Sum of first {0} numbers is {1}", a, sum, Thread.CurrentThread.Name);
    }

    public static void productation(int b)
    {
        int pdt = 1;
        for (int i = 1; i < b; i++)
        {
            pdt *= i;
        }
        Console.WriteLine("{2} Says: Product of first {0} numbers is {1}", b, pdt, Thread.CurrentThread.Name);
    }

    public static void printArray(int[] arr)
    {
        Console.Write('[');
        for (int i = 0; i < arr.Length; i++)
        {
            Console.Write("{0}, ", arr[i]);
        }
        Console.Write(']');
    }
    public static int arraySum(int[] arr)
    {
        int sum = 0;
        foreach (int num in arr)
        {
            sum += num;
            Thread.Sleep(500);
        }
        return sum;
    }

    public static void countDown(int num){
        if (num <=0 ) return;
        while (num>0){
            Console.WriteLine("{0}", num);
            num -= 1;
            Thread.Sleep(1000);
        }
        Console.WriteLine("{0}",num);
    }
}