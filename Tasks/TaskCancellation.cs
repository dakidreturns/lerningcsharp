using System;
using System.Threading;
using System.Threading.Tasks;

namespace TasksExample;

internal class LongComputationClass
{

}

internal class TasksCancellation
{
    public static readonly int HUGE_NUMBER = 1000;
    public static async Task DoParallelStuffAsync()
    {
        CancellationTokenSource tokenSource2 = new CancellationTokenSource();
        CancellationToken ct = tokenSource2.Token;

        Task task = Task.Run(() =>
        {
            ct.ThrowIfCancellationRequested(); // Check for already cancelled.
            int i = 0;
            bool workRemaining = true;
            while (workRemaining && i < HUGE_NUMBER)
            {
                Sample.summation(i);
                i += 1;

                if (ct.IsCancellationRequested)
                {
                    Console.WriteLine("Oh no I was running in Thread {0} {1} I was cancelled.", Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);
                    // return;
                    ct.ThrowIfCancellationRequested();
                    return;
                }

            }
        }, ct);

        while (task.Status != TaskStatus.RanToCompletion)
        {
            if (task.Status == TaskStatus.Faulted || task.Status == TaskStatus.Canceled) break;

            Console.WriteLine("Cancel Task? ");

            string? userInput = Console.ReadLine();
            if (userInput == null)
            {
                Console.WriteLine("Error user input recived.");
                break;
            }
            if (userInput.ToLower().Equals("y"))
            {
                Console.WriteLine("Cancelling task..");
                tokenSource2.Cancel();
                break;
            }
        }
        try
        {
            tokenSource2.CancelAfter(1000);
            await task;
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine($"{nameof(OperationCanceledException)} thrown with message: {e.Message}");
            Console.WriteLine("Task Status: {0}", task.Status);
        }
        finally
        {
            tokenSource2.Dispose();

            if (task.Status == TaskStatus.RanToCompletion)
            {
                Console.WriteLine("Yay The task ran to completion");
            }
            else if (task.Status == TaskStatus.Canceled)
            {
                Console.WriteLine("Oh No It was cancelled");
            }
            else if (task.Status == TaskStatus.Faulted)
            {
                Console.WriteLine("Oh oh how unfortuante... it stopped due to execption");
            }
        }
    }

    public static async Task UsingKeywordForToken()
    {
        using CancellationTokenSource ct = new CancellationTokenSource();
        Console.WriteLine("Printing thread name: {0}", Thread.CurrentThread.Name);

        Task t1 = Task.Factory.StartNew(() =>
        {
            while (!ct.IsCancellationRequested)
            {
                SomeWorkForCraig();
                Console.WriteLine("{0} Working... Working...", Thread.CurrentThread.Name);
                ct.Token.ThrowIfCancellationRequested();
            }
        });


        Console.WriteLine("Press any key to quit");
        Console.ReadLine();
        Console.WriteLine("Exiting... Cleaning up......");

        try
        {
            Task completed = await Task.WhenAny(t1, Task.Delay(10000)); // await for 10 seconds.
            if (completed != t1)
            {
                Console.WriteLine("Oh no the task didnt finish waiting for it to cancel");
                ct.Cancel();
            }
            // await t1;
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine("Recived Execption: Over and out; {0}", e.Message);
        }
    }

    private static void SomeWorkForCraig()
    {
        Thread.Sleep(1000);
    }
}