﻿using System;
internal class Program
{
    public static void Main(string[] args)
    {
        // DelegateExample.ThingToDo(SampleClass.WriteSomething);
        // DelegateExample.ThingToDo(SampleClass.WriteSomething);
        // DelegateExample.ThingToDo(SampleClass.WriteSomething);
        // DelegateExample.DoTheThing("The Greatest in the world");

        //ActionBasics.ThingToDo(delegate (string name) {Console.WriteLine("This is some gourmet {0}", name) ; });
        DelegateExample.ThingToDo(SampleClass.WriteSomething);
        //ActionBasics.DoTheThing(": Greatest in the world");
        
        FuncBasics.addToFunc(delegate (string data) { return data.Length; });
        FuncBasics.addToFunc(SampleClass.CountWords);
        
        DelegateExample.DoTheThing(FuncBasics.getTheThing("Hello is there life?").ToString());

    }
}