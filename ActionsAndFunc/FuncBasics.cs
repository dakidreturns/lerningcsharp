using System;

public class FuncBasics{
    private static Func<string, int>? funcThatTaksString;

    public static void addToFunc( Func<string, int> funcToAdd){
        if (funcThatTaksString == null){
            funcThatTaksString = funcToAdd;
        }else{
            funcThatTaksString += funcToAdd;
        }
        
    }

    public static int getTheThing( string input){
        
        return funcThatTaksString?.Invoke(input) ?? -1;
    }

}