using System;

/*
Actions are predefined delegates that do not return a value.
*/

public class ActionBasics
{
    public static Action<string>? theThing;
    public static void ThingToDo(Action<string> callback)
    {
        if (theThing == null)
        {
            theThing = callback;
        }
        else
        {
            theThing += callback;
        }
    }
    public static void DoTheThing(string parameter)
    {
        if (theThing == null)
        {
            return;
        }
        theThing(parameter);
    }
}