using System;
class SampleClass{
    public static void SomeText(){
        Console.WriteLine("Hello line");
    }

    public static void WriteSomething(string text){
        Console.WriteLine(text);
    }

    public static int CountWords(string text){
        int count = 0;
        foreach ( char c in text){
            if (c == ' '){
                count += 1;
            }
        }
        return count+1;
    }
    public static int charsInWords(string text){
        return text.Length;
    }
}