using System;

public delegate void TheThingThatNeedsToBeDone(string theThing);
public class DelegateExample
{
    private static TheThingThatNeedsToBeDone? theThing;

    public delegate int DelegateThatGives(string data);
    // private static DelegateThatGives? delegateThatGives; 
    
    public static void ThingToDo(TheThingThatNeedsToBeDone callback)
    {
        if (theThing == null)
        {
            theThing = callback;
        }
        else
        {
            theThing += callback;
        }
    }
    public static void DoTheThing(string parameter)
    {
        if (theThing == null){
            return;
        }
        theThing(parameter);
    }
}